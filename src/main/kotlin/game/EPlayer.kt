package game

import com.diogonunes.jcolor.AnsiFormat
import com.diogonunes.jcolor.Attribute

enum class EPlayer(val score: Int, val format: AnsiFormat) {
    X(1, AnsiFormat(Attribute.GREEN_TEXT())),
    O(-1, AnsiFormat(Attribute.RED_TEXT()))
}