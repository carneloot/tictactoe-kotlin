package ai

class Cursor(var node: Node) {

    fun traverse(position: Int) {
        this.node = this.node.children.find { it.position == position } as Node
    }

    fun getBestPosition(): Int {
        return this.node.bestChild!!.position!!
    }

}