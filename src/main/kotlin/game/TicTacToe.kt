package game

import com.diogonunes.jcolor.Ansi
import com.diogonunes.jcolor.AnsiFormat
import com.diogonunes.jcolor.Attribute

class TicTacToe {
    private val state: GameState

    companion object {
        val boardFormat = AnsiFormat(Attribute.WHITE_TEXT())
        val neutralPlayerFormat = AnsiFormat(Attribute.YELLOW_TEXT())
    }

    constructor() {
        this.state = GameState(
            null,
            EPlayer.X,
            arrayOf(arrayOfNulls(3), arrayOfNulls(3), arrayOfNulls(3)),
            (0..8).toMutableSet()
        )
    }

    private constructor(state: GameState) {
        this.state = state
    }

    fun showBoard() {
        var placeIndicator = 0

        for (i in 0..2) {
            for (j in 0..2) {

                print(" ")

                val positionValue = this.state.board[i][j]

                if (positionValue == null) {
                    print(Ansi.colorize(placeIndicator.toString(), neutralPlayerFormat))
                } else {
                    print(Ansi.colorize(positionValue.name, positionValue.format))
                }

                print(" ")

                placeIndicator++

                if (j < 2) {
                    print(Ansi.colorize("|", boardFormat))
                }
            }

            println()
            if (i < 2) {
                println(Ansi.colorize("---+---+---", boardFormat))
            }
        }

        println()
        println()
    }

    fun play(position: Int): TicTacToe {
        if (this.hasGameEnded()) {
            throw GameEndedException()
        }

        if (!this.state.availablePlaces.contains(position)) {
            throw PositionNotAvailableException()
        }

        val newGame = TicTacToe(this.state.copy())

        newGame.makePlay(position)

        return newGame
    }

    fun hasGameEnded(): Boolean {
        // Draw
        if (this.state.availablePlaces.size == 0) {
            return true
        }

        return this.state.winner !== null
    }

    fun getWinner(): EPlayer? {
        return this.state.winner
    }

    fun getAvailablePlaces(): List<Int> {
        return this.state.availablePlaces.toList()
    }

    private fun makePlay(position: Int) {
        val i = position / 3
        val j = position % 3

        this.state.board[i][j] = this.state.currentPlayer
        this.state.availablePlaces.remove(position)

        this.checkWinner()

        this.state.invertCurrentPlayer()

    }

    private fun checkWinner() {
        // Rows
        for (i in 0..2) {
            if (this.state.board[i][0] == this.state.currentPlayer &&
                this.state.board[i][1] == this.state.currentPlayer &&
                this.state.board[i][2] == this.state.currentPlayer
            ) {
                this.state.winner = this.state.currentPlayer
                return
            }
        }

        // Columns
        for (j in 0..2) {
            if (this.state.board[0][j] == this.state.currentPlayer &&
                this.state.board[1][j] == this.state.currentPlayer &&
                this.state.board[2][j] == this.state.currentPlayer
            ) {
                this.state.winner = this.state.currentPlayer
                return
            }
        }

        // Diagonals
        if (this.state.board[0][0] == this.state.currentPlayer &&
            this.state.board[1][1] == this.state.currentPlayer &&
            this.state.board[2][2] == this.state.currentPlayer
        ) {
            this.state.winner = this.state.currentPlayer
            return
        }

        if (this.state.board[0][2] == this.state.currentPlayer &&
            this.state.board[1][1] == this.state.currentPlayer &&
            this.state.board[2][0] == this.state.currentPlayer
        ) {
            this.state.winner = this.state.currentPlayer
            return
        }
    }

}