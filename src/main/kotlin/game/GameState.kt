package game

data class GameState(
    var winner: EPlayer?,
    var currentPlayer: EPlayer,

    val board: Array<Array<EPlayer?>>,
    val availablePlaces: MutableSet<Int>
) {
    fun invertCurrentPlayer() {
        this.currentPlayer = if (this.currentPlayer == EPlayer.X) EPlayer.O else EPlayer.X
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GameState

        if (winner != other.winner) return false
        if (currentPlayer != other.currentPlayer) return false
        if (!board.contentDeepEquals(other.board)) return false
        if (availablePlaces != other.availablePlaces) return false

        return true
    }

    override fun hashCode(): Int {
        var result = winner?.hashCode() ?: 0
        result = 31 * result + currentPlayer.hashCode()
        result = 31 * result + board.contentDeepHashCode()
        result = 31 * result + availablePlaces.hashCode()
        return result
    }

    fun copy(): GameState {
        return GameState(
            this.winner,
            this.currentPlayer,
            arrayOf(
                this.board[0].toList().toTypedArray(),
                this.board[1].toList().toTypedArray(),
                this.board[2].toList().toTypedArray()
            ),
            this.availablePlaces.toMutableSet()
        )
    }
}