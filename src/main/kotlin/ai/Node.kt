package ai

class Node(private val type: ENodeType, val position: Int?) {
    val children: MutableList<Node> = emptyList<Node>().toMutableList()

    var bestChild: Node? = null
    var score: Float? = null

    fun addChild(child: Node) {
        this.children.add(child)

        if (this.bestChild == null) {
            this.bestChild = child
        } else {

            when (this.type) {
                ENodeType.MAX -> if (child.score!!.compareTo(this.bestChild!!.score!!) > 0) this.bestChild = child
                ENodeType.MIN -> if (child.score!!.compareTo(this.bestChild!!.score!!) < 0) this.bestChild = child
            }

        }

        this.score = this.bestChild!!.score
    }

    override fun toString(): String {
        return "Node(type=${type.name}, position=$position, score=$score)"
    }
}
