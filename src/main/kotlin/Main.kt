import ai.ENodeType
import ai.MinimaxTree
import com.diogonunes.jcolor.Ansi
import game.TicTacToe
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

@ExperimentalTime
fun main() {
    println("Starting...")

    // Generating tree and calculating time it took to generate
    val (tree, elapsed) = measureTimedValue {
        MinimaxTree(ENodeType.MAX)
    }

    println("Tree created in ${elapsed.inMilliseconds}ms!")

    var userInput = ""
    while (userInput != "Y" && userInput != "N") {
        println("Human starts? [Y/n]")
        userInput = readLine()!!.toUpperCase()

        if (userInput == "") {
            userInput = "Y"
        }
    }

    val humanStarts = userInput == "Y"

    println("The game will keep repeating. Interrupt the terminal to stop.")

    while (true) {
        var game = TicTacToe()
        val cursor = tree.getCursor()

        var move: Int

        if (!humanStarts) {
            move = cursor.getBestPosition()
            game = game.play(move)
            cursor.traverse(move)
        }

        while (!game.hasGameEnded()) {
            game.showBoard()

            move = readLine()!!.toInt()
            game = game.play(move)
            cursor.traverse(move)

            if (game.hasGameEnded()) {
                break
            }

            move = cursor.getBestPosition()
            game = game.play(move)
            cursor.traverse(move)
        }

        game.showBoard()

        if (game.getWinner() == null) {
            println("It's a ${Ansi.colorize("draw", TicTacToe.neutralPlayerFormat)}!")
        } else {
            val winner = Ansi.colorize(game.getWinner()!!.name, game.getWinner()!!.format)
            println("The winner is $winner")
        }

        println("Press enter to continue...")
        readLine()

    }
}

