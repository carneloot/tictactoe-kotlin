package ai

import game.TicTacToe

class MinimaxTree(rootType: ENodeType) {

    private val root: Node

    init {
        val game = TicTacToe()
        root = createNode(game, null, rootType, 0)
    }

    fun getCursor(): Cursor {
        return Cursor(root)
    }

    companion object {

        private fun createNode(game: TicTacToe, position: Int?, nodeType: ENodeType, layer: Int): Node {
            val currentNode = Node(nodeType, position)

            if (game.hasGameEnded()) {
                currentNode.score = (game.getWinner()?.score ?: 0) / layer.toFloat()

                return currentNode
            }

            val newType = if (nodeType == ENodeType.MAX) ENodeType.MIN else ENodeType.MAX

            for (playingPosition in game.getAvailablePlaces()) {
                val playedGame = game.play(playingPosition)

                val node = createNode(playedGame, playingPosition, newType, layer + 1)

                currentNode.addChild(node)
            }

            return currentNode
        }

    }

}