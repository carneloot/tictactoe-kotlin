package game

import java.lang.Exception

class GameEndedException : Exception("The game already ended.")

class PositionNotAvailableException : Exception("Position is not available.")